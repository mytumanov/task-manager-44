package ru.mtumanov.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.model.IWBS;
import ru.mtumanov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "tm_project")
@Getter
@Setter
@NoArgsConstructor
public final class ProjectDTO extends AbstractUserOwnedModelDTO implements IWBS {

    @NotNull
    @Column(nullable = false)
    private String name = "";

    @Nullable
    @Column(nullable = true)
    private String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private Status status = Status.NOT_STARTED;

    @NotNull
    @Column(nullable = false)
    private Date created = new Date();

    public ProjectDTO(@NotNull final String name, @NotNull final String description, @NotNull final Status status) {
        this.name = name;
        this.description = description;
        this.status = status;
    }

    @Override
    public String toString() {
        return "ID:" + id + " " +
                "NAME:" + name + " " +
                "DESCRIPTION:" + description + " " +
                "STATUS:" + getStatus().getDisplayName() + " " +
                "USER ID: " + getUserId() + "\n";
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof ProjectDTO)) {
            return false;
        }
        ProjectDTO project = (ProjectDTO) o;
        return Objects.equals(name, project.name)
                && Objects.equals(description, project.description)
                && Objects.equals(status, project.status)
                && Objects.equals(created, project.created)
                && super.equals(o);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, description, status, created);
    }

}