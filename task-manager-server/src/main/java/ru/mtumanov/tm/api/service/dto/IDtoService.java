package ru.mtumanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.model.AbstractModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import java.util.Collection;
import java.util.List;

public interface IDtoService<M extends AbstractModelDTO> {

    @NotNull
    List<M> findAll();

    @NotNull
    void set(@NotNull Collection<M> models) throws AbstractException;

}
