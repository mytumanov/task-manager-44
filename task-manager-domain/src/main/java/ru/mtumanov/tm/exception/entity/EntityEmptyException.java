package ru.mtumanov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public class EntityEmptyException extends AbstractException {

    public EntityEmptyException() {
        super("ERROR! Entity empty!");
    }

    public EntityEmptyException(@NotNull String entity) {
        super("ERROR! Entity:" + entity + " empty!");
    }

}
