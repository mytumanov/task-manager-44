package ru.mtumanov.tm.component;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public final class Backup {

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        executorService.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void save() {
        try {
            bootstrap.getDomainService().saveDataBase64();
        } catch (@NotNull final Exception e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void load() {
        try {
            bootstrap.getDomainService().loadDataBase64();
        } catch (@NotNull final Exception e) {
            bootstrap.getLoggerService().error(e);
        }
    }

    public void stop() {
        executorService.shutdown();
    }

}
