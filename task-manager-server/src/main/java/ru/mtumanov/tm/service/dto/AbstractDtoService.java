package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.dto.IDtoService;
import ru.mtumanov.tm.dto.model.AbstractModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public abstract class AbstractDtoService<M extends AbstractModelDTO, R extends IDtoRepository<M>> implements IDtoService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    protected final R repository;

    protected AbstractDtoService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
        repository = getRepository();
    }

    @NotNull
    private EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected abstract R getRepository();

    @Override
    @NotNull
    public List<M> findAll() {
        return repository.findAll();
    }

    @Override
    public void set(@NotNull final Collection<M> models) throws AbstractException {
        for (M model : models) {
            repository.add(model);
        }
    }

}
