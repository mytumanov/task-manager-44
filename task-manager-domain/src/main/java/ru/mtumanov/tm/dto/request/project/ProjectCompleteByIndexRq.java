package ru.mtumanov.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.request.AbstractUserRq;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCompleteByIndexRq extends AbstractUserRq {

    @Nullable
    private Integer index;

    public ProjectCompleteByIndexRq(@Nullable final String token, @NotNull final Integer index) {
        super(token);
        this.index = index;
    }

}
