package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.Comparator;
import java.util.List;

public class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IDtoProjectRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    public ProjectDtoRepository(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM ProjectDTO WHERE userId = :userId")
                    .setParameter(USER_ID, userId)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        }
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM ProjectDTO p WHERE p.userId = :userId", ProjectDTO.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll(@NotNull final String userId, @NotNull final Comparator<ProjectDTO> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM ProjectDTO p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator), ProjectDTO.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public ProjectDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id", ProjectDTO.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM ProjectDTO p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM ProjectDTO p WHERE p.userId = :userId AND p.id = :id")
                    .setParameter(USER_ID, userId)
                    .setParameter(ID, id)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        try {
            findOneById(userId, id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

    @Override
    public void clear() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM ProjectDTO")
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    @NotNull
    public List<ProjectDTO> findAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM ProjectDTO p", ProjectDTO.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<ProjectDTO> findAll(@NotNull final Comparator<ProjectDTO> comparator) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("FROM ProjectDTO p ORDER BY p." + getComporator(comparator), ProjectDTO.class)
                .getResultList();
    }

    @Override
    @Nullable
    public ProjectDTO findOneById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.find(ProjectDTO.class, id);
    }

    @Override
    public long getSize() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        return entityManager.createQuery("SELECT COUNT(1) FROM ProjectDTO p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            entityManager.createQuery("DELETE FROM ProjectDTO WHERE id = :id")
                    .setParameter(ID, id)
                    .executeUpdate();
            transaction.commit();
        } catch (@NotNull final Exception e) {
            transaction.rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existById(@NotNull final String id) {
        try {
            findOneById(id);
        } catch (@NotNull final NoResultException e) {
            return false;
        }
        return true;
    }

}
