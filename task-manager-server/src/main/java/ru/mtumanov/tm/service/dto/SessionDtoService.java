package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.dto.IDtoSessionRepository;
import ru.mtumanov.tm.api.service.IConnectionService;
import ru.mtumanov.tm.api.service.dto.IDtoSessionService;
import ru.mtumanov.tm.dto.model.SessionDTO;
import ru.mtumanov.tm.repository.dto.SessionDtoRepository;

public class SessionDtoService extends AbstractDtoUserOwnedService<SessionDTO, IDtoSessionRepository> implements IDtoSessionService {

    public SessionDtoService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @NotNull
    protected IDtoSessionRepository getRepository() {
        return new SessionDtoRepository(connectionService);
    }

}
